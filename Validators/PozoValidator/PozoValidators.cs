﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Validators.PozoValidator
{
    public class PozoValidators
    {
        public virtual bool Pass(Pozo pozo)
        {
            if (String.IsNullOrEmpty(pozo.CoordenadaOeste))
                return false;
            if (String.IsNullOrEmpty(pozo.CordenadaEste))
                return false;
            if (String.IsNullOrEmpty(pozo.Nombre))
                return false;
            if (String.IsNullOrEmpty(pozo.Porpietario))
                return false;
            if (String.IsNullOrEmpty(pozo.Ubicacion))
                return false;
            return true;
        }
    }
}
