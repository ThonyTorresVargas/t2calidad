﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Interfaces;
using Modelos;
using Validators.PozoValidator;

namespace T2Calidad.Controllers
{
    public class PozoPetroleroController : Controller
    {
        //
        // GET: /PozoPetrolero/
        private InterfacePozo repository;
        private PozoValidators validator;

        public PozoPetroleroController(InterfacePozo repository, PozoValidators validator)
        {
            this.repository = repository;
            this.validator = validator;
        }

        [HttpGet]
        public ViewResult Index(String query = "")
        {
            var datos = repository.ByQueryAll(query);
            return View("CreatePozo",datos);
        }

        [HttpGet]
        public ViewResult Create()
        {
            return View("RegistrarPozo");
        }

        [HttpPost]
        public ActionResult Create(Pozo pozo)
        {

            if (validator.Pass(pozo))
            {
                repository.Store(pozo);

                TempData["UpdateSuccess"] = "Se creó correctamente";

                return RedirectToAction("Index");
            }

            return View("ListaDePozos", pozo);
        }

        [HttpPost]
        public ViewResult AsignarPozoAPersona(Pozo pozo, Persona persona)
        {
            var datos = repository.AsignarPersonaAPozo(pozo, persona);
            return View("AsignarPozoAPersona", datos);
        }

    }
}
