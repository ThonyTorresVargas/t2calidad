﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Interfaces
{
    public interface InterfacePozo
    {
        List<Pozo> All();
        List<Pozo> ByQueryAll(string query);
        void Store(Pozo pozo);

        Pozo AsignarPersonaAPozo(Pozo pozo, Persona persona); //He considerado que me retorna el pozo con la perzona y se mostrarán enlazados
        List<Pozo> TraerPozosConPersonas();//Traerá la lista con las personas enlazadas
    }
}
