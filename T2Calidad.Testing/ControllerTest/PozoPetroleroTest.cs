﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Modelos;
using NUnit.Framework;
using System.Web.Mvc;
using Interfaces;
using T2Calidad.Controllers;
using Validators.PozoValidator;

namespace T2Calidad.Testing.ControllerTest
{
    [TestFixture]
    public class PozoPetroleroTest
    {
        [Test]
        public void RetornaVista()
        {
            var mock = new Mock<InterfacePozo>();
            mock.Setup(o => o.All()).Returns(new List<Pozo>());
            mock.Setup(o => o.ByQueryAll("")).Returns(new List<Pozo>());

            var controller = new PozoPetroleroController(mock.Object, null);
            var view = controller.Index();

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("CreatePozo", view.ViewName);
            Assert.IsInstanceOf(typeof(List<Pozo>), view.Model);
        }

        //Pregunta 1

        [Test]
        public void TestCreateReturnView()
        {
            var controller = new PozoPetroleroController(null, null);

            var view = controller.Create();

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("RegistrarPozo", view.ViewName);
        }


        [Test]
        public void TestPostCreateOKReturnRedirect()
        {
            var pozo = new Pozo();

            var repositoryMock = new Mock<InterfacePozo>();

            repositoryMock.Setup(o => o.Store(new Pozo()));

            var validatorMock = new Mock<PozoValidators>();

            validatorMock.Setup(o => o.Pass(pozo)).Returns(true);

            var controller = new PozoPetroleroController(repositoryMock.Object, validatorMock.Object);

            var redirect = (RedirectToRouteResult)controller.Create(pozo);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), redirect);
        }

        [Test]
        public void TestPostCreateCallStoreMethodFromRepository()
        {
            var pozo = new Pozo();

            var repositoryMock = new Mock<InterfacePozo>();

            var validatorMock = new Mock<PozoValidators>();

            validatorMock.Setup(o => o.Pass(pozo)).Returns(true);

            repositoryMock.Setup(o => o.Store(pozo));

            var controller = new PozoPetroleroController(repositoryMock.Object, validatorMock.Object);

            var redirect = controller.Create(pozo);

            repositoryMock.Verify(o => o.Store(pozo), Times.Once());
        }

        public void TestPostCreateReturnViewWithErrorsWhenValidationFail()
        {
            var pozo = new Pozo { };

            var mock = new Mock<PozoValidators>();

            mock.Setup(o => o.Pass(pozo)).Returns(false);

            var controller = new PozoPetroleroController(null, mock.Object);

            var view = controller.Create(pozo);

            Assert.IsInstanceOf(typeof(ViewResult), view);
        }

        //Pregunta 2

        [Test]
        public void TestAsignarPersonaAPozo()
        {
            var pozo = new Pozo { };

            var mock = new Mock<InterfacePozo>();
            var persona = new Persona { Id=1, Nombre="Persona 1"};
            mock.Setup(o => o.All()).Returns(new List<Pozo>());
            mock.Setup(o => o.ByQueryAll("")).Returns(new List<Pozo>());
            mock.Setup(o => o.AsignarPersonaAPozo(pozo, persona)).Returns(new Pozo());

            var mockv = new Mock<PozoValidators>();

            mockv.Setup(o => o.Pass(pozo)).Returns(false);

            var controller = new PozoPetroleroController(mock.Object, mockv.Object);

            var view = controller.AsignarPozoAPersona(pozo,persona);

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("AsignarPozoAPersona", view.ViewName);
            Assert.IsInstanceOf(typeof(Pozo), view.Model);
        }

        [Test]
        public void TestCreateEnlaceReturnView()
        {
            var controller = new PozoPetroleroController(null, null);

            var view = controller.Create();

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("RegistrarPozo", view.ViewName);
        }


    }
}
