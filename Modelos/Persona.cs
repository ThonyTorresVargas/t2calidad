﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Persona
    {
        public Int32 Id { get; set; }
        public String Nombre { get; set; }
    }
}
