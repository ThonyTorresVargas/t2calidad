﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Pozo
    {
        public String Nombre { get; set; }
        public String Porpietario { get; set; }
        public String Ubicacion { get; set; }
        public String CordenadaEste { get; set; }
        public String CoordenadaOeste { get; set; }
    }
}
